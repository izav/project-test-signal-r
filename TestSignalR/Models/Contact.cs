﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using TestSignalR.Infrastructure;


namespace TestSignalR.Models
{
    public class Contact
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }

        [Required]
        [MaxLength(30)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(30)]
        [Display(Name = "Last Name")]       
        public string LastName { get; set; }

        [Required]
        [MaxLength(30)]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}", ErrorMessage = "Not a valid Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Your must provide a Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        [StringLength(13, MinimumLength = 10)]
        public string Phone { get; set; }

        [Required]
        public int AddressId { get; set; }       
        public Address Address { get; set; }
    }
}