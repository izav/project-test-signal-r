﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSignalR.Models
{
    public class ContactLog
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string Action { get; set; }
        public DateTime DateTime { get; set; }
       
    }
}