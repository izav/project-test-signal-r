﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSignalR.Repositories;
using TestSignalR.Models;
using TestSignalR.ViewModels;
using TestSignalR.Hubs;
using Microsoft.AspNet.SignalR;
using System.IO;
using Hangfire;

namespace TestSignalR.Controllers
{
    public class HomeController : Controller
    {
        private IContactRepository<Contact, int> _repository;


        public static string CRON_STRING_UPDATE_NAME = "0-59 * * * *";

        public ActionResult AboutTest() 
        {
            // test "Simple MVC Contact Manager"
            string fileName = "CodingProjectforFull-StackDeveloper.pdf";
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/", "");
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + fileName);
            return File(fileBytes, "application/octet-stream", fileName);
        } 

        public static void UpdateNameEvery1Min()
        {
            var db = new DbContextEF();
            IContactRepository<Contact, int> repository = new ContactRepository(db);
            repository.UpdateNamesForContact();

            var jobId = BackgroundJob.Schedule(() => UpdateNameFrom30Sec(), TimeSpan.FromSeconds(30));
        }

        public static void UpdateNameFrom30Sec()
        {
            var db = new DbContextEF();
            IContactRepository<Contact, int> repository = new ContactRepository(db);
            repository.UpdateNamesForContact();

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNames()
        {
            var names = _repository.GetNames()
                           .Select(c => new {Name = c.LastName +" "+ c.FirstName, UpdatedName = c.Name?? ""}).ToList();
           

            return Json(
            new
            {
                names = names,  
            },
            JsonRequestBehavior.AllowGet);
        }
        

        public HomeController(IContactRepository<Contact, int> repository)
        {
            _repository = repository;
           
        }



        public ActionResult Index(int pageNo = 1, int pageSize = 20)
        {
            var contacts = _repository.GetContacts( pageNo, pageSize);
            return View(contacts);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetContacts(int pageNo = 1, int pageSize = 20)
        {
           
            var contacts = _repository.GetContacts(pageNo, pageSize);
            return Json(
            new
            {
                contacts = contacts
            },
            JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetContactsBy(string id, int pageNo = 1, int pageSize = 20)
        {
            var pattern = id;
            var contacts = _repository.GetContactsBy(pattern, pageNo, pageSize);
            return Json(
            new
            {
                contacts = contacts
            },
            JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetContact(int id)
        {
             var contact = _repository.GetContact(id);
             var info = _repository.GetContactLogInfo_SP(id);
            
            return Json(
            new
            {
                contact = contact,
                count = info.UpdateCount,
                time = info.LastUpdate.ToString("dd-MMM-yyyy HH:mm:ss")
            },
            JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveContact(Contact Contact, string ConnectId)
        {
            var isNewContact = Contact.Id == 0;
            Contact savedContact = new Contact();
            if (ModelState.IsValid) {
                savedContact = _repository.SaveContact(Contact) ?? new Contact();                
            }

            if (savedContact.Id > 0)
            {
                if (isNewContact) ChatHub.AddContact(ConnectId, savedContact);
                else ChatHub.UpdateContact(ConnectId, savedContact);
            }
            return Json(
             new
             {
                 contact = savedContact             
             },
             JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteContact(int id, string ConnectId)
        {
            var result = _repository.DeleteContact(id);
            if (id > 0 )
                ChatHub.DeleteContact(ConnectId, id.ToString());
            return Json(
            new
            {
               result = result                
            },
            JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetContactLogs(int id)
        {
            var info = _repository.GetContactLogs(id);
            return Json(
            new
            {
               
                logs = info.Select(l => new {
                    DateTime = l.DateTime.ToString("dd-MMM-yyyy HH:mm:ss"),
                    Action = l.Action
                })
                
            },
            JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetContactInfo(int id)
        {
            var info = _repository.GetContactLogInfo_SP(id);
            return Json(
            new
            {
                count = info.UpdateCount,
                time = info.LastUpdate.ToString("dd-MMM-yyyy HH:mm:ss")
            },
            JsonRequestBehavior.AllowGet);
        }

        
        
        public ActionResult IsUID_Available(string FirstName)
        {
            return Json(_repository.CheckName(FirstName.Trim()), JsonRequestBehavior.AllowGet);
               
        } 

    }
}
