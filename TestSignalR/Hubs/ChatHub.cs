﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using TestSignalR.Models;

namespace TestSignalR.Hubs
{
    public class ChatHub : Hub
    {
        static List<User> Users = new List<User>();


        public void Connect(string userName)
        {
            var id = Context.ConnectionId;

            if (Users.Count(x => x.ConnectionId == id) == 0)
            {
               
                Users.Add(new User { ConnectionId = id, Name = userName });
                Clients.Caller.onConnected(id, userName, Users);
                Clients.AllExcept(id).onNewUserConnected(id, userName);
            }
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id, item.Name);
            }

            return base.OnDisconnected(stopCalled);
        }
       

        public void Delete(string name, string message)
        {

            Clients.All.deleteContactFromList(name, message);

        }

        public static void DeleteContact(string connectId, string message)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            context.Clients.AllExcept(connectId).deleteContactFromList(connectId, message);
        }


        public static void AddContact(string connectId, Contact contact)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            context.Clients.AllExcept(connectId).addContact(connectId, contact);
        }


        public static void UpdateContact(string connectId, Contact contact)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            context.Clients.AllExcept(connectId).updateContact(connectId, contact);
        }

        
    }
}