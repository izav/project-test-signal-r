﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using Ninject;
using TestSignalR.Repositories;
using TestSignalR.Models;

namespace TestSignalR.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            AddBindingsDB();
        }

        protected override IController GetControllerInstance(RequestContext requestContext,
                                    Type controllerType)
        {
            return controllerType == null
            ? null
            : (IController)kernel.Get(controllerType);
        }


        private void AddBindingsDB()
        {
            kernel.Bind<IContactRepository<Contact, int>>().To<ContactRepository>();
        }
    }
}