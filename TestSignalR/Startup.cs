﻿using Owin;
using Hangfire;
using Hangfire.Dashboard;
using TestSignalR.Repositories;
using TestSignalR.Controllers;
using TestSignalR.Filters;
using Microsoft.Owin;
[assembly: OwinStartup(typeof(TestSignalR.Startup))]


namespace TestSignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            using (var context = new DbContextEF())
            {
                // db must be created
                context.Database.Initialize(false);
            }
            app.MapSignalR();

            SetJobs(app);
        }

        private void SetJobs(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard("/Hangfire", new DashboardOptions()
            {
                Authorization = new[] { new HangFireAuthorizationFilter() }
            });
            app.UseHangfireServer();
            RecurringJob.AddOrUpdate(() => HomeController.UpdateNameEvery1Min(), HomeController.CRON_STRING_UPDATE_NAME);
        }
    }
}