﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSignalR.ViewModels
{
    public class ContactLogInfo
    {
        public DateTime LastUpdate { get; set; }
        public int UpdateCount { get; set; }
    }
}