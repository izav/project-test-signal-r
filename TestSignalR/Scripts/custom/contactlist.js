﻿
$(document).ready(function () {
            
           
            $(".container").on("click", ".btn-save-contact", function (event) {
               
                var form = $(this).closest('form');
                var placeHolder = $('[data-valmsg-for="Address"]');

                        form.validate();
                        if (!form.valid()) {  return false };

                        var geocoder = new google.maps.Geocoder();
                        var currentAddress =
                                   $("#Address_Street").val() + ", " +
                                   $("#Address_City").val() + ", " +
                                   $("#Address_State").val() + "  " +
                                   $("#Address_PostalCode").val() +", USA";
                        
                        geocoder.geocode({ 'address': currentAddress }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                               
                                
                                for (var i = 0; i < results.length; i++) {
                                    if (results[i].partial_match == null) {
                                        var address = results[i].formatted_address.trim().split(",");
                                        var address2 = address[2].trim().split(" ");
                                        
                                        $("#Address_Street").val(address[0]);
                                        $("#Address_City").val(address[1]);
                                        $("#Address_State").val(address2[0]);
                                        if (address2.length > 1) // if (state=nyc+ correct zip) google return  state witout zip
                                        {
                                            $("#Address_PostalCode").val(address2[1]);
                                        }
                                        
                                            
                                        //alert(results[i].formatted_address);
                                       
                                        break;
                                    }
                                    else {
                                        placeHolder.html('Google is not able to locate address:<br/>"' + currentAddress + '"')
                                                 .removeClass('field-validation-valid').addClass('field-validation-error');
                                        return;
                                    }
                                }

                        $.post("/Home/SaveContact", form.serialize(), function (data) {

                            var dt = data.contact;
                            if (dt.Id == 0) {
                                alert("Error at server");
                                
                            }
                            else {

                                var dt = data.contact;

                                if ($("#Id").val() != '0') //if edit contact
                                {
                                    //find row for update
                                    $(".link-delete-contact").each(function () {

                                        if ($(this).attr("href").split('/')[3] == dt.Id) {
                                            var target = $(this).closest('.row').children();

                                            target.eq(0).find(".span-contact-name").text(dt.LastName + " " + dt.FirstName);
                                            target.eq(1).html(dt.Phone + "<br/>" + dt.Email);

                                            var dtt = dt.Address;
                                            target.eq(2).html(dtt.Street + "<br/>" + dtt.City + " " + dtt.State + " " + dtt.PostalCode);

                                        };
                                    });
                                }
                                else //if add contact
                                {
                                    //insert new row
                                    var newControl = $("#tmpContacts").tmpl({ "contacts": [dt] });
                                    $("#hr-contacts").after(newControl);
                                }

                            }
                        });

                        $('#frmContactModal').modal('toggle');
                        return false;

                    }
                    else {

                        alert("Google is not able to locate address");
                    }
                });

            });

            
           //=============================
            $(".container").on("click", ".btn-info-names", function (event) {
                event.preventDefault();
               
                $.post("/Home/GetNames", function (data) {
                  
                    var temp ="Hangfire job every 30 sec<br/><table><tr><th></th><th><b><u>Name</u></b></th><th><b><u>Updated Name</u></b></th></tr>"
                    $.each(data.names, function (index, value) {

                        var name = value.UpdatedName == "" ?  "<b>" + value.Name +"</b>" : value.Name ;
                        temp += "<tr><td style='padding-right:20px;'>" + (index + 1) +
                                    "<td style='padding-right:20px;'>" + name +
                                       "</td><td>" + value.UpdatedName + "</td></tr>";
                    });
                    temp += "</table>";

                    BootstrapDialog.alert({ 
                        title: 'Info',
                        message: temp,
                        type: BootstrapDialog.TYPE_INFO,
                    });
                    return false;
                });

            });
            //=============================
            $(".container").on("click", ".link-info-contact", function (event) {
                event.preventDefault();
                var linkObj = $(this);
                var parent = linkObj.closest('.row');
                var name = parent.find(".span-contact-name").html();
               
               
                $.post(linkObj[0].href, function (data) {
                    var temp = "Logs for   " + name + "<br/><table>";
                    $.each(data.logs, function (index, value) {
                       
                        if (value.Action == "I") {
                            temp += "<tr><td><b>Created:</b></td></tr><tr><td></td><td>" + value.DateTime + "</td></tr>"
                        }
                        else {
                           
                            temp += index == 1 ? "<tr><td><b>Updated:</b></td><td></td></tr>" : "";
                            temp += "<tr><td>" + index + "</td><td>" + value.DateTime + "</td></tr>";
                        }
                        
                    })
                    temp += "</table>";
                    
                    BootstrapDialog.alert({
                        title: 'Info',
                        message: temp,
                        type: BootstrapDialog.TYPE_INFO,
                    });
                    return false;
                });

            });

           
            //=============================
            $(".container").on("click", ".btn-add-contact", function (event) {
                event.preventDefault();
                    //clear errors
                    var form = $("#frmContactModal").find('form');
                    $inputs = form.find("input").removeClass("input-validation-error");
                    $('.field-validation-error').empty();

                    $("#FirstName").val("");
                    $("#LastName").val("");
                    $("#Email").val("");
                    $("#Phone").val("");
                    $("#Id").val(0);
                    $("#AddressId").val(0);
                    $("#Address_Id").val(0);
                    $("#Address_Street").val("");
                    $("#Address_City").val("");
                    $("#Address_State").val("");
                    $("#Address_PostalCode").val("");
                    $(".modal-title").text("New contact");
                    $("#div-info").empty();
                    $("#frmContactModal").modal('show');
                return false;

            });
            
            //=============================
            $(".container").on("click", ".link-edit-contact", function (event) {
                event.preventDefault();

                //clear errors
                var form = $("#frmContactModal").find('form');
                $inputs = form.find("input").removeClass("input-validation-error");
                $('.field-validation-error').empty();
                
                var linkObj = $(this);
                var parent = linkObj.closest('.row');
                var name = parent.find(".span-contact-name").html();
               
                $.post(linkObj[0].href, function (data) {                  
                    var dt = data.contact;
                    $("#FirstName").val(dt.FirstName);
                    $("#LastName").val(dt.LastName);
                    $("#Email").val(dt.Email);
                    $("#Phone").val(dt.Phone);
                    $("#Id").val(dt.Id);
                    $("#AddressId").val(dt.AddressId);
                    $("#Address_Id").val(dt.Address.Id);
                    $("#Address_Street").val(dt.Address.Street);
                    $("#Address_City").val(dt.Address.City);
                    $("#Address_State").val(dt.Address.State);
                    $("#Address_PostalCode").val(dt.Address.PostalCode);
                    $(".modal-title").text("Edit contact");

                    var count = data.count;
                    var info = (count == 1 ? "<b>Created: </b> " : "<b>Update times :</b> " + (count - 1) + "<br/><b>Last update :</b> ")
                                  + data.time;

                    $("#div-info").html(info);
                    $("#frmContactModal").modal('show'); 
                });

               
                return false;

            });


            //=============================
            $(".container").on("click", ".btn-refresh-search", function (event) {
                event.preventDefault();
                //clear search control
                var cnt = $(this).closest('.row').find('input').val("");
                var target = $("#hr-contacts");
                var jqxhr = $.post("/Home/GetContacts", function (data) {

                    //delete old rows
                    var controlsToDelete = $(".link-delete-contact").closest(".row");
                    controlsToDelete.next().remove();
                    controlsToDelete.remove();

                    // template, add new rows
                    var newControl = $("#tmpContacts").tmpl(data);
                    target.after(newControl);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert("Error  " + xhr.responseText);
                });

                return;
            });

            //=============================
            $(".row").on("click", ".btn-search-contacts", function (event) {
                event.preventDefault();
                var btn = $(this);
                var value = btn.closest('.input-group').find('input').val().trim();
               
                if (value == "")
                {
                    BootstrapDialog.alert({
                        title: 'Warning',
                        message: $("<div>Search string can't be empty</div>"),
                        type: BootstrapDialog.TYPE_WARNING,
                    });
                    return false;
                }

                // template
                var target = $("#hr-contacts");

                    var jqxhr = $.post("/Home/GetContactsBy/" + value, function (data) {
                        var controlsToDelete = $(".link-delete-contact").closest(".row");
                        controlsToDelete.next().remove();
                        controlsToDelete.remove();
                        var newControl = $("#tmpContacts").tmpl(data);
                        target.after(newControl);
                    })

                    .fail(function (xhr, textStatus, errorThrown) {

                        alert("Error  " + xhr.responseText);
                    });

                    return;
                });

           
            //===================================
            $(".container").on("click", ".link-delete-contact", function (event) {
                    event.preventDefault();
                   
                    var deleteLinkObj = $(this);
                    var parent = deleteLinkObj.closest('.row');
                    var name = parent.find(".span-contact-name").html();

                    var temp = "<div id ='divModalDelete'>Do you want to delete contact for " + name +"</div>";
                  

                    BootstrapDialog.show({
                        title: 'Confirmation',
                        message: temp,

                        buttons: [{
                            label: 'Yes',
                            action: function (dialogItself) {

                                $.post(deleteLinkObj[0].href, { 'ConnectId': $('#hdId').val() }, function (data) {
                                    if (data.result > 0) {
                                        parent.next().remove();
                                        parent.remove();
                                        dialogItself.close();
                                    }
                                    else {

                                        dialogItself.close();
                                        temp.find("#divModalDelete").html("Not able to delete contact  for" + name).addClass("text-danger");
                                        var dialogInstance1 = new BootstrapDialog({
                                            title: 'Error',
                                            type: BootstrapDialog.TYPE_DANGER,
                                            message: temp.html()
                                        });
                                        dialogInstance1.open();
                                    }

                                });
                            }

                        }, {
                            label: 'No',
                            action: function (dialogItself) {
                                dialogItself.close();
                            }
                        }]
                    });


                });



        });