﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestSignalR.Models;
using TestSignalR.ViewModels;

namespace TestSignalR.Repositories
{
    public interface IContactRepository<T, TId> where T : class
    {
        void UpdateNamesForContact();
        IEnumerable<Contact> GetNames();
        IEnumerable<Contact> GetContacts(int pageNo, int pageSize);
        IEnumerable<Contact> GetContactsBy(string pattern, int pageNo, int pageSize);
        Contact GetContact(int id);
        Contact SaveContact(Contact contact);
        IEnumerable<ContactLog> GetContactLogs(int id);
        ContactLogInfo GetContactLogInfo_SP(int id);
        int DeleteContact(int id);
        bool CheckName(string name);
    }
}