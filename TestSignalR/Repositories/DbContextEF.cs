﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestSignalR.Models;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace TestSignalR.Repositories
{
    public class DbContextEF : DbContext
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactLog> ContactLogs { get; set; }

        public DbContextEF()
            : base("DefaultConnection") { }

        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Address>()
           .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Contact>()
           .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ContactLog>()
            .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}