﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using TestSignalR.Models;
using System.Text;

namespace TestSignalR.Repositories
{
   public class DbContextEFInitializer : DropCreateDatabaseIfModelChanges<DbContextEF>
    {
        protected override void Seed(DbContextEF context)
        {
            AddSeed(context);
           
            base.Seed(context);
        }

        private void AddSeed(DbContextEF context)
        {
            // PROCEDURE
            context.Database.ExecuteSqlCommand(
                        @"CREATE PROCEDURE [dbo].[sp_GetContactLogInfo] (@id int)
                        AS
                        BEGIN
                        SET NOCOUNT ON;
                        SELECT  Max(DateTime) as LastUpdate, Count(*) as UpdateCount
                        FROM ContactLogs
                        Where ContactId = @id 
                        END;");

            //Triggers (for update EF)
             context.Database.ExecuteSqlCommand(
                          @"CREATE TRIGGER trg_UpdateInsert ON Contacts After INSERT
                          AS
                             DECLARE @Now DATETIME
                             SET @Now = GETUTCDATE()  
                             INSERT into ContactLogs (DateTime, ContactId, Action) 
                             SELECT @Now, Id, 'I'  FROM INSERTED");

            

            context.Database.ExecuteSqlCommand(
                        @"CREATE TRIGGER trg_DeleteContacts ON Contacts After DELETE 
                          AS
                             DECLARE @Now DATETIME
                             SET @Now = GETUTCDATE()  
                             INSERT into ContactLogs (DateTime, ContactId, Action) 
                             SELECT @Now, Id, 'D'  FROM DELETED");

            
            

            // Data
            context.Contacts.Add(new Contact
            {
                FirstName = "Taras",
                LastName = "Shevchenko",
                Email = "Shevchenko@yahoo.com",
                Phone = "717 456 7416",
                Address = new Address { Street = "Nostrand Ave", City = "Brooklyn", State = "NY", PostalCode = "11235" }
            });

            context.Contacts.Add(new Contact
            {
                FirstName = "Mihai",
                LastName = "Eminescu",
                Email = "Eminescu@yahoo.com",
                Phone = "717 432 7416",
                Address = new Address { Street = "Avenue S", City = "Brooklyn", State = "NY", PostalCode = "11234" }
            });

            context.Contacts.Add(new Contact
            {
                FirstName = "Pablo",
                LastName = "Neruda",
                Email = "Neruda@yahoo.com",
                Phone = "717 646 3333",
                Address = new Address { Street = "Nostrand Ave", City = "Brooklyn", State = "NY", PostalCode = "11234" }
            });

            context.Contacts.Add(new Contact
            {
                FirstName = "Oscar",
                LastName = "Wilde",
                Email = "Wilde@yahoo.com",
                Phone = "717 555 7416",
                Address = new Address { Street = "Avenue W", City = "Brooklyn", State = "NY", PostalCode = "11235" }
            });

            context.Contacts.Add(new Contact
            {
                FirstName = "Robert",
                LastName = "Frost",
                Email = "Frost@yahoo.com",
                Phone = "717 646 8888",
                Address = new Address { Street = "Avenue Z", City = "Brooklyn", State = "NY", PostalCode = "11235" }
            });

            context.Contacts.Add(new Contact
            {
                FirstName = "Sylvia",
                LastName = "Plath",
                Email = "Plath@yahoo.com",
                Phone = "717 567 7416",
                Address = new Address { Street = "Avenue Y", City = "Brooklyn", State = "NY", PostalCode = "11235" }
            });

             context.Contacts.Add(new Contact
            {
                FirstName = "Allen",
                LastName = "Ginsberg",
                Email = "Ginsberg@yahoo.com",
                Phone = "717 646 7416",
                Address = new Address { Street = "Nostrand Ave", City = "Brooklyn", State = "NY", PostalCode = "11235" }
            });

             context.Contacts.Add(new Contact
            {
                FirstName = "Sandor",
                LastName = "Petofi",
                Email = "Petofi@yahoo.com",
                Phone = "737 646 7416",
                Address = new Address { Street = "Avenue X", City = "Brooklyn", State = "NY", PostalCode = "11235" }
            });

             context.Contacts.Add(new Contact
            {
                FirstName = "Paul",
                LastName = "Verlaine",
                Email = "Verlaine@yahoo.com",
                Phone = "717 656 7416",
                Address = new Address { Street = "Haring str", City = "Brooklyn", State = "NY", PostalCode = "11235" }
            });

            context.SaveChanges();
            
        }
    
    }
}