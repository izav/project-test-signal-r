﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestSignalR.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


using System.ComponentModel.DataAnnotations.Schema;

//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;




using TestSignalR.ViewModels;
using System.Data.SqlClient;
using System.Data;




namespace TestSignalR.Repositories
{
    public class ContactRepository : IContactRepository<Contact, int>
    {

        private DbContextEF _context;
        public ContactRepository(DbContextEF context)
        {
            _context = context;
        }

        public void UpdateNamesForContact()
        {
            var contacts = _context.Contacts.Where(c => c.Name == null).ToList();
            foreach (var e in contacts)
            {
                 e.Name = e.LastName + " " + e.FirstName;
                _context.Entry(e).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }

        public IEnumerable<Contact> GetNames()
        {
            IEnumerable<Contact> contacts = _context.Contacts.OrderBy(c => c.LastName)
                    .ToList();
            return contacts;
        }

        public IEnumerable<Contact> GetContacts(int pageNo, int pageSize) 
        {
            IEnumerable<Contact> contacts = _context.Contacts.OrderBy(c => c.LastName)                   
                    .Skip(pageSize * (pageNo - 1))
                    .Take(pageSize)
                    .Include(c => c.Address)
                    .ToList();
            return contacts;
        }

        public IEnumerable<Contact> GetContactsBy(string pattern, int pageNo, int pageSize)
        {
            IEnumerable<Contact> contacts = _context.Contacts
                    .Where(k => k.LastName.StartsWith(pattern))
                    .OrderBy(c => c.LastName)
                    .Skip(pageSize * (pageNo - 1))
                    .Take(pageSize)
                    .Include(c => c.Address)
                    .ToList();
            return contacts;
        }


        public Contact GetContact(int id)
        {
            Contact contact = _context.Contacts.Where(c => c.Id == id)
                    .Include(c => c.Address)
                    .FirstOrDefault();     
            return contact;
        }


        public IEnumerable<ContactLog> GetContactLogs(int id)
        {
            var infos = _context.ContactLogs
                       .Where(c => c.ContactId == id)
                       .OrderBy(d => d.DateTime);
            return infos;
        }



        public ContactLogInfo GetContactLogInfo_SP (int id)
        {
           SqlParameter spParam = new SqlParameter("@id", id);
           ContactLogInfo info = null;
           info = _context.Database.SqlQuery<ContactLogInfo>("sp_GetContactLogInfo @id ", spParam).FirstOrDefault();
           return info;
        }




        public int DeleteContact(int id)
        {   
           try
            {                   
                   var original =_context.Contacts.Find(id);
                   var originalAddress = _context.Addresses.Find(original.AddressId);

                   if (original != null)
                   {
                       _context.Entry(originalAddress).State = EntityState.Deleted;
                       _context.Entry(original).State = EntityState.Deleted;
                       _context.SaveChanges();
                   }
                   else return 0;
            }
            catch
            {
                id = -1;
            }
            return id;
        }




        public Contact SaveContact(Contact contact)
        {
            Contact original = contact;
            try
            {
                if (contact.Id == 0)
                {   // add contact and address
                    _context.Entry(original).State = EntityState.Added;
                   
                }
                else
                {
                   original = _context.Contacts.Find(contact.Id);

                    if (original != null)
                    {
                        //update 2 tables
                        var originalAddress = _context.Addresses.Find(contact.AddressId);
                        _context.Entry(originalAddress).CurrentValues.SetValues(contact.Address);
                        _context.Entry(original).CurrentValues.SetValues(contact);

                        //Save to log or in SQL instead of trigger for view of 2 tables
                        var log = new ContactLog { Action = "U", ContactId = original.Id, DateTime = DateTime.UtcNow };
                        _context.Entry(log).State = EntityState.Added;
                        
                    }
                    else return original;
                }

                _context.SaveChanges();
 
            }
            catch
            {
                original = null;
            }
            return original;
        }


        public bool CheckName(string name)
        {
            Contact contact = _context.Contacts.Where(c => c.FirstName == name)
                    .FirstOrDefault();
            return  contact == null;
        }
         
    }
}