# **Simple Contact Manager **

**[http://tst2018-001-site1.btempurl.com](http://tst2018-001-site1.btempurl.com)**



*(Open the project in several windows and enter any user name.  If link doesn't work below screemshots)
*
---------------------

Build a very simple contact management MVC application using C#.

** Applications:**

* Visual Studio 2013 or 2015 (C# .net 4.5 or lower)
* SQL Server 2014 (or lower)


**Contact fields:**

* Name (FirstName, LastName)
* Phone
* Email
* Address (Street, City, State, Postal Code)



**Objectives:**

* Use Entity Framework to interface with DB
* Search, list Contact records
* Allow user to Add, edit, delete Contact records. For Edit Use AJAX (no full posts) from a View dedicated to Contact Edit
* Use a SQL Trigger to log changes and deletes to Contact records
* SQL Procedure (via Entity Framework method) to return number of changes and last updated date/time for the current Contact Record. Display on the Contact Edit window.



**Bonus points for any of the following (not required):**

* Use any third-party API (Google Maps, USPS, etc.) to validate/verify the address
* Implement Hangfire.io. Create a new field DisplayName. Schedule a job that runs every 30 seconds that updates DisplayName (from FirstName and LastName) for any new Contact records.
* Implement SignalR. Synchronize Contact record edits across all open browser windows.



_______________________________________________________________________________







## **Used software** 


**Languages**

* C#, JavaScript

**Database**

* MS SQL Server 2014

**ORM**

* Entity Framework 6 (Code First)

**Background processing**

* Hangfire

**Internet**

* ASP.NET MVC 4, SignalR, JavaScript, jQuery,  Templates, Ajax, CSS

**Responsive web design**

* front-end web framework - Bootstrap 3

**API**

* Google Maps


_____________________________________________________________

## **Screenshots** 



**1) Use any third-party API (Google Maps, USPS, etc.) to validate/verify the address**

*Google Maps* 

_____________________________________________________________

![Test5.jpg](https://bitbucket.org/repo/Ggnekzp/images/4131760593-Test5.jpg)

_____________________________________________________________



 **2) Implement Hangfire.io. Create a new field DisplayName. Schedule a job that runs every 30 seconds that updates DisplayName (from FirstName and LastName) for any new Contact records.**



*Hangfire Dashboard*

_____________________________________________________________


![Test6.jpg](https://bitbucket.org/repo/Ggnekzp/images/2711258537-Test6.jpg)


_____________________________________________________________


*Minimum interval for Hangfire - 1 minute(!)*

*'UpdateNameEvery1Min' - Cron job that is executing every minute and runing 'UpdateNameFrom30Sec', another job  with 30 seconds delay.*



_____________________________________________________________

![Test7.jpg](https://bitbucket.org/repo/Ggnekzp/images/3064308477-Test7.jpg)




_____________________________________________________________



*A new contact was added*

_____________________________________________________________



![Test3.jpg](https://bitbucket.org/repo/Ggnekzp/images/814074895-Test3.jpg)

_____________________________________________________________



*30 seconds after  a new contact was added*


_____________________________________________________________



![Test4.jpg](https://bitbucket.org/repo/Ggnekzp/images/2878994757-Test4.jpg)


_____________________________________________________________


**3) Implement SignalR. Synchronize Contact record edits across all open browser windows.**


*Hub with 3 users*

_____________________________________________________________


![S1.jpg](https://bitbucket.org/repo/Ggnekzp/images/1325359055-S1.jpg)

_____________________________________________________________


*Phone number is going to be changed.*
_____________________________________________________________

![S3.jpg](https://bitbucket.org/repo/Ggnekzp/images/1579828379-S3.jpg)

_____________________________________________________________

*After updating*
_____________________________________________________________

![S5.jpg](https://bitbucket.org/repo/Ggnekzp/images/3902077666-S5.jpg)

_____________________________________________________________

** Return number of changes and last updated date/time for the current Contact Record**

_____________________________________________________________ 

![S6.jpg](https://bitbucket.org/repo/Ggnekzp/images/584654517-S6.jpg)

_____________________________________________________________